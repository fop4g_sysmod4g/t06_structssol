#include <iostream>
#include <string>
#include <assert.h>

using namespace std;

/*
* Solution to tutorial question about introducing structs
*/

void GetText(const string&, string&, bool);
int GetNum(const string&, int, int);

/*
Student details
*/
struct Student
{
	string fName, sName; //first and last name
	int IDNum;	//student ID

	/*
	Get the student's details
	precon	- none
	postcon	- name and ID are set
	*/
	void GetDetails() {
		fName = "";
		sName = "";
		IDNum = 0;
		GetText("Enter your first name and press return:", fName, true);
		GetText("Enter your surname name and press return:", sName, true);
		IDNum = GetNum("Enter your student ID (a number between 1000 and 9999:", 1000,9999);
	}

	/*
	Display a summary of the student details
	*/
	void ShowDetails()
	{
		cout << "\n\nStudent summary";
		cout << "\n---------------";
		cout << "\nName: " << fName << " " << sName;
		cout << "\nID: " << IDNum;
		cout << "\n\n";
	}
};


int main()
{
	cout << "\n\nStructs";

	Student s;
	s.GetDetails();
	s.ShowDetails();

	system("pause");
	return 0;
}

/*
Get some text from the user
msg			- IN some instructions to display
val			- OUT user entry
firstCapital- capitalise first letter
*/
void GetText(const string& msg, string& val, bool firstCapital)
{
	assert(val.empty());
	assert(!msg.empty());
	bool inputOK;
	do {
		inputOK = true;
		cout << "\n\n" << msg;
		cin >> val;
		if (cin.fail() || val.empty())
		{
			cout << "\nBad input, try again";
			cin.clear();
			cin.ignore(10000, '\n');
			inputOK = false;
		}
	} while (!inputOK);
	
	if(!val.empty())
		val[0] = toupper(val[0]);
}

/*
Get a number from the user
msg		- IN some instructions to display
min/max	- IN upper and lower limits
RETURN	- the number entered
*/
int GetNum(const string& msg, int min, int max)
{
	assert(!msg.empty());
	int val=-1;
	do {
		cout << "\n\n" << msg;
		cin >> val;
		if (cin.fail() || val<min || val>max)
		{
			cout << "\nBad input, try again";
			cin.clear();
			cin.ignore(10000, '\n');
			val = -1;
		}
	} while (val==-1);
	return val;
}
